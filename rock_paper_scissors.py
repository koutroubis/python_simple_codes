#!/bin/python2

import random, os	#random module is for the computer move and os module is for the clear command

class colors:
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'


os.system('clear')	#clear terminal

print "Hello!\nLet's play rock paper scissors.\n"

player_score = computer_score = 0	#initialize scores

while(1):
	try:	
		player = raw_input("What do you choose? ").lower()	#take user input and convert to lowercase

		moves = ['rock', 'paper', 'scissors']	#valid moves

		computer = random.choice(moves) #pick a random computer move
		
		if player not in moves:		#if the player choice was invalid
			raise Exception

		elif player == computer: 	#tie if computer and player made the same choice
			print colors.YELLOW + "\nIt's a tie!\n" + colors.ENDC
		
		elif (player == 'rock' and computer == 'paper') or (player == 'paper' and computer == 'scissors') or (player == 'scissors' and computer == 'rock'):	#combinations that the player loses
			print colors.RED + "\nYou lose!\n" + colors.ENDC
			computer_score += 1		#increase computer score by 1

		else:
			print colors.GREEN + "\nYou win!\n" + colors.ENDC
			player_score += 1		#increase player score by 1

		print "The computer chose %s" % computer

		print colors.BOLD + "\nThe score is:\nPlayer: %d\nComputer: %d\n" % (player_score,computer_score) + colors.ENDC 	#print the score
		
		raw_input("Press any key to continue playing or CTRL-C to stop\n")		#ask if the player wants to continue playing

	except(KeyboardInterrupt):		#handle exception when user uses CTRL-C
		print colors.BLUE + "\n\nThank you for playing!\n" + colors.ENDC	
		break

	except:
		print colors.YELLOW+ "\nInvalid choice. Your choices are rock, paper, scissors.\n" + colors.ENDC 	#handle exception when user's move is invalid
