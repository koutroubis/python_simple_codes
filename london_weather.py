#!/usr/bin/python2

import urllib2,json


url = "http://api.openweathermap.org/data/2.5/find?q=London&type=accurate&mode=json&units=metric&APPID=e2778a5030348ab8da4bd8ab90b43c62"

flag = True

while flag:
	try:
		weather = urllib2.urlopen(url).read()
	except: continue
	flag = False

data = json.loads(weather)

city = data['list'][0]['name']

description = data['list'][1]['weather'][0]['description']

temp = data['list'][1]['main']['temp']


print "\nCity: " + city
print "Description: " + description
print "The temperature is: " + str(temp) + " C\n"

