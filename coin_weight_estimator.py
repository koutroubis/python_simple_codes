#!/usr/bin/python2

import math

def num_of_pennies(pennies):
	weight = 2.5
	pennies = pennies/weight
	wrappers = math.floor(pennies / 50)
	print "You have %d pennies and you will need %d wrappers.\n" % (int(pennies),wrappers)

def num_of_nickels(nickels):
	weight = 5
	nickels = nickels/weight
	wrappers = math.floor(nickels / 40)
	print "You have %d nickels and you will need %d wrappers.\n" % (int(nickels),wrappers)

def num_of_dimes(dimes):
	weight = 2.268
	dimes = dimes/weight
	wrappers = math.floor(dimes / 50)
	print "You have %d dimes and you will need %d wrappers.\n" % (int(dimes),wrappers)

def num_of_quarters(quarters):
	weight = 5.67
	quarters = quarters/weight
	wrappers = math.floor(quarters / 40)
	print "You have %d quarters and you will need %d wrappers.\n" % (int(quarters),wrappers)


pennies = float(raw_input("\nHow many grams your pennies weigh? "))
nickels = float(raw_input("\nHow many grams nickels weigh? "))
dimes = float(raw_input("\nHow many grams your dimes weigh? "))
quarters = float(raw_input("\nHow many grams your quarters weigh? "))

num_of_pennies(pennies)
num_of_nickels(nickels)
num_of_dimes(dimes)
num_of_quarters(quarters)
