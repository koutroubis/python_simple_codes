#!/usr/bin/python2

import os,urllib2,re
os.system('clear')

hangman= ["""\
    ________________
    |               |
    |               
    |
    |
    |
    |
    |
    |
    |
    |
    |
    |
    |
    |
    |
____|____
""","""\
    ________________
    |               |
    |              ***
    |             *   *
    |             *   *
    |              ***
    |
    |
    |
    |
    |
    |
    |
    |
    |
    |
____|____
""","""\
     ________________
    |               |
    |              ***
    |             *   *
    |             *   *
    |              ***
    |               |
    |               |
    |               |
    |               |
    |               |
    |               |
    |
    |
    |
    |
____|____
""","""\
     ________________
    |               |
    |              ***
    |             *   *
    |             *   *
    |              ***
    |               |
    |               |
    |               |
    |               |
    |               |
    |               |
    |              / 
    |             /   
    |            /  
    |
____|____
""","""\
     ________________
    |               |
    |              ***
    |             *   *
    |             *   *
    |              ***
    |               |
    |               |
    |               |
    |               |
    |               |
    |               |
    |              / \\
    |             /   \\
    |            /     \\
    |
____|____
""","""\
     ________________
    |               |
    |              ***
    |             *   *
    |             *   *
    |              ***
    |               |  /
    |               | /
    |               |/
    |               |
    |               |
    |               |
    |              / \\
    |             /   \\
    |            /     \\
    |
____|____
""", """\
     ________________
    |               |
    |              ***
    |             *   *
    |             *   *
    |              ***
    |            \\  |  /
    |             \\ | /
    |              \\|/
    |               |
    |               |
    |               |
    |              / \\
    |             /   \\
    |            /     \\
    |
____|____
"""
]

flag = True

lang = raw_input("Choose g/G for Greek or e/E for English: ").lower()

while True:	
	
	os.system('clear')

	if lang == "e":
		page = urllib2.urlopen("http://creativitygames.net/random-word-generator/randomwords/1").read() #get random word from a site
		word = re.search(r'randomword_1">[a-z]*', page).group(0).strip('randomword_1">').strip('<')
	
	if lang == "g":
		test = urllib2.urlopen("http://el.thefreedictionary.com/").read()
		word = re.search(r'style="padding-right:22px"><a href=".*>', test).group(0).strip('randomword_1">').strip('<')
		p = urllib2.unquote(word).decode('utf8')
		word = re.search(r'style="padding-right:22px"><a href=".*>', p).group(0).strip('style="padding-right:22px"><a href="').split('">')[0]
	
	word = list(word.lower())

	if len(word) < 5 : continue

	
	chosen = []
	mistakes = 0
	incorrect = []
		
	printed_word = [' _ ' for i in word]
	print hangman[mistakes]
	print "\n" + "".join(printed_word)
	
	while flag:
		
		printed_word= []

		while True:
			choice = raw_input("\n\nPick next letter: ").lower()
			if lang == "g":
				choice = unicode(choice, "utf-8")
			if choice == "" : print "You must choose a letter. " ; continue
			break
		
		if choice in chosen: print "You have already picked " + choice + " try again."; continue

		if len(choice)>2:
			print "\nAre you trying to cheat? You must choose one letter!\n"
			continue

		if choice == u"\u03b1":
			chosen.append(u"\u03ac")

		elif choice == u"\u03b5":
			chosen.append(u"\u03ad")

		elif choice == u"\u03c3":
			chosen.append(u"\u03c2")

		elif choice == u"\u03c2":
			chosen.append(u"\u03c3")

		elif choice == u"\u03b9":
			chosen.append(u"\u03af")
			chosen.append(u"\u03ca")

		elif choice == u"\u03b7":
			chosen.append(u"\u03ae")

		elif choice == u"\u03bf":
			chosen.append(u"\u03cc")

		elif choice == u"\u03c9":
			chosen.append(u"\u03ce")
	
		elif choice == u"\u03c5":
			chosen.append(u"\u03b0")
			chosen.append(u"\u03cd")
			chosen.append(u"\u03cb")


		chosen.append(choice)
	
		print chosen
		if word.count(choice) == 0: mistakes += 1; incorrect.append(choice)
	
		for i in range(len(word)):
			if word[i] not in chosen:
				printed_word.append(" _ ")
			else:
				printed_word.append(word[i])
	
		os.system('clear')
	
		print hangman[mistakes]	
		print "The incorrect letters you have picked are: " + ",".join(incorrect)
	
		if mistakes == (len(hangman)-1): 
			print "GAME OVER!\n"
			print "The word was: " + "".join(word) + "\n"
			if not(raw_input("Press Enter to play again or any other key to exit: ") == ""): exit(0)
			break
	
		if printed_word == word:
			print "\nWELL DONE!!!\nThe word was:",
			print "".join(printed_word) + "\n"
			if not(raw_input("Press Enter to play again or any other key to exit: ") == ""): exit(0) 
			break
			
		print " ".join(printed_word)
