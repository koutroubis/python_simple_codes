#!/usr/bin/python2

flag = True

while flag:
	
	try:
		seq = raw_input("\nPlease enter a number sequence using comma to separate the numbers.\n\n")
	
		seq = map(float,seq.split(','))
	
		mean = sum(seq)/len(seq)
		
		seq.sort()
		
		if (len(seq) % 2) == 0:
			median = (seq[len(seq)/2]+seq[len(seq)/2-1])/2
		else:
			median = seq[len(seq)/2]
	
		b = {}
	
		for i in list(set(seq)):
		    b[seq.count(i)] = i
		
		mode = b[max(b)]
		
		print "The mean is: %f" % mean
		print "The mode is: %d" % mode
		print "The median is: %f" % median

		flag = False

	except KeyboardInterrupt:
		print "\n\nGoodbye!\n"
		exit(0)
	except:
		print "Wrong sequence format. Please try again.\n"
