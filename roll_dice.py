#!/usr/bin/python2

import random

def roll_dice(num):
	"""
	Takes as input the number of dice that the
	user wants to roll and returns the random rolls
	"""
	result = [random.randint(1,6) for i in range(num)]
	print "\nYou rolled " + " and ".join(map(str,result))
	print "\nYour total is %d" % sum(result) 

def start():
	"""
	Function that executes whet program starts or 
	when user wants to change the number of dice to be rolled
	"""
	start.dice_num = raw_input("\nHow many dice do you want to roll? ")
	if start.dice_num.isdigit(): roll_dice(int(start.dice_num))
	else: print "You should pick a number."

start()

while(1):
	try:
		user_input = raw_input("\nPress ENTER to roll again,\nPress CTRL-C to quit or,\nPress a to change the number of dice to roll\n")
	except (KeyboardInterrupt):
		print "\nGoodbye!\n"
		break
	
	if user_input.lower() == 'a':
		start()
	else:
		roll_dice(int(start.dice_num))
