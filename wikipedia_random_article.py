#!/usr/bin/python2

import urllib2, webbrowser, ast, os

os.system('clear')

print "\nWelcome to the Wikipedia Random Article Api\n"
print """Press y/Y if you want to read an article
Press any other key to continue.
Press CTRL-C to exit.\n """

while(1):

	try:
		articles = urllib2.urlopen("http://en.wikipedia.org/w/api.php?action=query&list=random&rnnamespace=0&rnlimit=10&format=json").read()	#get wikipedia random article api
		articles = ast.literal_eval(articles)	#convert from string to dictionaries
		lst = articles['query']['random']	#access the dictionaries
		
		for i in range(0,10):
			print "Would you like to read about \"" + lst[i]['title'].decode('unicode-escape') + "\" ?" #get every article's title
			choice = raw_input()
			url = "http://en.wikipedia.org/wiki?curid=" + str(lst[i]['id'])

			if choice.lower() == 'y': 
				webbrowser.open(url)	#open url in browser

	except (KeyboardInterrupt):
		print "\n\nThank you for using Wikipedia Random Article Api\n"
		exit(0)
