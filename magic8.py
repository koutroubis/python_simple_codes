#!/usr/bin/python2

import time,random,sys,os

os.system("clear")

answers = ["It is certain",
"It is decidedly so",
"Without a doubt",
"Yes definitely",
"You may rely on it",
"As I see it, yes",
"Most likely",
"Outlook good",
"Yes",
"Signs point to yes",
"Reply hazy try again",
"Ask again later",
"Better not tell you now",
"Cannot predict now",
"Concentrate and ask again",
"Don't count on it",
"My sources say no",
"My reply is no",
"Outlook not so good",
"Very doubtful"]

flag = True

while flag:

	raw_input("\nAsk a question to the Magic 8 Ball.\n")
	
	print "\nThinking",
	for i in range(6):
		sys.stdout.write(".")
		sys.stdout.flush()
		time.sleep(0.5)
	
	print "\n\n" + answers[random.randint(0,len(answers)-1)] + "\n"
	
	again = raw_input("Ask again? (Y/N)")
	if again.lower() == "n": flag = False; print "\nThank you for playing!\n\n"
	elif again.lower() =="y": flag = True
	else: print "I will take that as a yes."


