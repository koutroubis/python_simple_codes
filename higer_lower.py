#!/usr/bin/python2

import time,sys,random

print """
Hello! Let's play the Higher-Lower game!

The Rules are simple.
I will select a number between 0 and 100 and you will have to guess it.
Each time you guess i will tell you if the number you are looking for is
higher or lower than the one you picked.

Have Fun!

"""

while True:

	sys.stdout.write("\nWhat number should I choose? Hmmmmmm")
	sys.stdout.flush()
	
	for i in range(4):
		sys.stdout.write(".")
		sys.stdout.flush()
		time.sleep(0.3)
	
	num = random.randint(1,99)
	
	print "\nOh I got it. Now it's your turn.\n"
	
	flag = True
	tries = 0
	
	while flag:
	
		try:
			choice = int(raw_input("What is your guess? "))
		except ValueError: continue
	
		if choice < num : print "\nThe number you seek is higher...\n"; tries += 1 ;continue
		elif choice > num : print "\nThe number you seek is lower...\n"; tries += 1 ; continue
		else : tries += 1 ; print "\nCongratulations! You found it! The number was %d and it took you %d attempts to find it!\n" % (num,tries); break;

	try:
		raw_input("Press any key to play again or CTRL-c to exit: ")
	except: print "\n\nThank you for playing! Goodbye!\n"; exit(0)
